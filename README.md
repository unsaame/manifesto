# MANIFESTO test

You will find here a "skeleton" for a Drupal 8 website used to answer the questions asked in the test.

Not all questions have been given an answer in code and I will try to explain in more details when this is not the case.

## Editorial experience:
> Client needs to be able to build the Article content type's content by adding one or more of the below components. Client needs the ability to move components up and down in the content and the ability to add a component multiple times.
Call To Action, Description + Button (link + label)
WYSIWYG
Image

I am using Paragraphs in order to implement the above and so as to make a content editor as pleasant as possible.
The field is called "Slice" and allows for three Paragraphs types:
- Call to action
- WYSIWYG
- Image

## Editorial experience / Permissions:
> If current editor role is not administrator, Article form Title field must have the description "Lorem ipsum dolor sit amet, consectetur adipiscing elit.". Description shouldn't appear for administrators.

I have created a custom module called manifesto_utilities. I have written a piece of code there which basically checks th form ID, then loads the current user and its role (see manifesto_utilities.module in /web/modules/custom/manifesto_utilities.module).

## Remote API, Drupal Backend and "I Feel Good" wildcard:
> Client needs the ability to fetch current weather for provided postcode from ABC service. This is needed across different components of the website, the client hasn't specified where yet. The candidate can either use a real service providing this if the candidate know any, mock-up the service output by HTTP fetching a json stored in the docroot or even skip the HTTP call completely and just return a weather value. Expected values is one of 'sunny', 'cloudy' or 'rainy'.

> Article must have Postcode field, where editor can set the London area the article is about. On the front end the Postcode displays right below the title and after the content a weather widget is displayed, showing the weather for the article postcode pulling the data from the service described above.

> Remember: Weather in London changes quickly! Quicker than Drupal's cache sometime... does the solution above consider it?

I thought it would be easier to group the last three tasks here. 
A custom block using ReactJS might be a good option. We could get the Postcode from the node through to Javascript with drupalSettings and make sure that the React component gets updated as often as possible.
I would use https://www.worldweatheronline.com/developer/api/docs/local-city-town-weather-api.aspx as it accepts UK postcodes.
